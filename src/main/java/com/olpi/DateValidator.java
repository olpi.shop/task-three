package com.olpi;

import java.time.LocalDate;

public class DateValidator {

    public boolean isYear(String text) {
        try {
            int year = Integer.parseInt(text);
            return year > 0;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public boolean isMonth(String text) {
        try {
            int month = Integer.parseInt(text);
            return month > 0 && month < 13;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public String toStringIsLeapYear(int year, int month) {

        if (!isYear(String.valueOf(year)) || !isMonth(String.valueOf(month))) {
            throw new IllegalArgumentException(
                    "There is an invalid value of year or month!");
        }

        LocalDate date = LocalDate.of(year, month, 1);

        return date.getYear() + " " + date.getMonth() + " - "
                + (date.isLeapYear() ? "leap year!" : "not leap year!");
    }

}
