package com.olpi;

import java.util.Scanner;

public class TaskThreeApp {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        DateValidator validator = new DateValidator();
        String initialText = "";
        int year = 0;
        int month = 0;

        System.out.println("Please enter a year: "
                + "\nOr enter \"Exit\" to close the program.");

        while (!initialText.equalsIgnoreCase("exit")) {

            initialText = scanner.nextLine();

            if (validator.isYear(initialText)) {
                year = Integer.valueOf(initialText);
                System.out.println(
                        "Please enter the month number (from 1 to 12): ");
                break;
            } else if (!initialText.equalsIgnoreCase("exit")) {
                System.out.println("You entered an invalid value, try again!");
            }
        }

        while (!initialText.equalsIgnoreCase("exit")) {

            initialText = scanner.nextLine();

            if (validator.isMonth(initialText)) {
                month = Integer.parseInt(initialText);

                System.out.println(validator.toStringIsLeapYear(year, month));

                break;
            } else if (!initialText.equalsIgnoreCase("exit")) {
                System.out.println("You entered an invalid value, try again!");
            }
        }

        scanner.close();
    }
}
